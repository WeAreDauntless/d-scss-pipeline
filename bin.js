#!/usr/bin/env node

/** @see docs.npmjs.com/files/package.json#bin */

//----------------------------------------------------------------------//
//                            ES strict mode                            //
//----------------------------------------------------------------------//

'use strict';


//----------------------------------------------------------------------//
//                         Imported NPM modules                         //
//----------------------------------------------------------------------//

const commandLineArgs = require('command-line-args');  // www.npmjs.com/package/command-line-args


//----------------------------------------------------------------------//
//                        Imported local modules                        //
//----------------------------------------------------------------------//

const SCSSPipeline = require('./main.js');


//----------------------------------------------------------------------//
//                              Constants                               //
//----------------------------------------------------------------------//

/** @see github.com/75lb/command-line-args/tree/v4.0.7#optiondefinition- */
const ARGS_CONFIG = [

    { name: 'globs',   type: String,  multiple: true, defaultOption: true },

    // Modes
    { name: 'execute', type: Boolean, defaultValue: false },
    { name: 'watch',   type: Boolean, defaultValue: false },

    // Actions
    { name: 'lint',    type: Boolean, defaultValue: false },
    { name: 'fix',     type: Boolean, defaultValue: false },
    { name: 'compile', type: Boolean, defaultValue: false }

];


//----------------------------------------------------------------------//
//                           Helper functions                           //
//----------------------------------------------------------------------//

function setExitErrorCode() {

    /** @see nodejs.org/dist/v8.9.0/docs/api/process.html#process_process_exitcode */
    process.exitCode = 1;

}

function verifyArgs(parsedArgs, actions) {

    if (!parsedArgs.execute && !parsedArgs.watch) {
        throw new Error('Must specify at least one of: --execute, --watch');
    }

    if (!actions.lint && !actions.fix && !actions.compile) {
        throw new Error('Must specify at least one of: --lint, --fix, --compile');
    }

}


//----------------------------------------------------------------------//
//                            Main function                             //
//----------------------------------------------------------------------//

function main() {

    try {

        /**
         * @see    github.com/75lb/command-line-args/tree/v4.0.7#api-reference
         * @throws {Error}
         */
        const parsedArgs = commandLineArgs(ARGS_CONFIG);

        const scssPipeline = new SCSSPipeline();

        const actions = {
            lint:    parsedArgs.lint,
            fix:     parsedArgs.fix,
            compile: parsedArgs.compile
        };

        /** @throws {Error} */
        verifyArgs(parsedArgs, actions);

        if (parsedArgs.execute) {

            scssPipeline.execute(parsedArgs.globs, actions)
                .catch(setExitErrorCode)
                .then(function () {
                    if (parsedArgs.watch) {
                        scssPipeline.watch(parsedArgs.globs, actions);
                    }
                });

        } else if (parsedArgs.watch) {

            scssPipeline.watch(parsedArgs.globs, actions);

        }

    } catch (e) {

        console.log('ERROR: ' + e.message);

        setExitErrorCode();

    }

}

main();
