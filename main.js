//----------------------------------------------------------------------//
//                            ES strict mode                            //
//----------------------------------------------------------------------//

'use strict';


//----------------------------------------------------------------------//
//                       Imported Node.js modules                       //
//----------------------------------------------------------------------//

const fs   = require('fs');    // nodejs.org/api/fs.html
const util = require('util');  // nodejs.org/api/util.html


//----------------------------------------------------------------------//
//                         Imported NPM modules                         //
//----------------------------------------------------------------------//

const chalk     = require('chalk');      // www.npmjs.com/package/chalk
const chokidar  = require('chokidar');   // www.npmjs.com/package/chokidar
const nodeGlob  = require('glob');       // www.npmjs.com/package/glob
const nodeSass  = require('node-sass');  // www.npmjs.com/package/node-sass
const stylelint = require('stylelint');  // www.npmjs.com/package/stylelint


//----------------------------------------------------------------------//
//                              Constants                               //
//----------------------------------------------------------------------//

/** @see api.postcss.org/CssSyntaxError.html */
const CSS_SYNTAX_ERROR = 'CssSyntaxError';

/** @see stylelint.io/user-guide/node-api/#options */
const STYLELINT_SYNTAX    = 'scss';
const STYLELINT_FORMATTER = 'string';

/** @see nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_readfile_path_options_callback */
const STYLELINT_INPUT_ENCODING = 'utf8';

const COMPILER_FILENAME_EXT_INPUT  = /\.scss$/;
const COMPILER_FILENAME_EXT_OUTPUT = '.css';


//----------------------------------------------------------------------//
//                          Promisified tools                           //
//----------------------------------------------------------------------//

const promisifiedNodeGlob  = util.promisify(nodeGlob);         // github.com/isaacs/node-glob/tree/v7.1.2#usage
const promisifiedNodeSass  = util.promisify(nodeSass.render);  // github.com/sass/node-sass/tree/v4.7.2#usage
const promisifiedReadFile  = util.promisify(fs.readFile);      // nodejs.org/dist/v8.9.0/docs/api/fs.html
const promisifiedWriteFile = util.promisify(fs.writeFile);     // nodejs.org/dist/v8.9.0/docs/api/fs.html


//----------------------------------------------------------------------//
//                           Helper functions                           //
//----------------------------------------------------------------------//

function conditionalExecute(condition, executor) {

    if (condition) {
        return executor();
    } else {
        return Promise.resolve();
    }

}

/**
 * @see stylelint.io/user-guide/node-api/#results
 * @see github.com/stylelint/stylelint/blob/8.4.0/decls/stylelint.js#L95-L100
 * @see github.com/stylelint/stylelint/blob/8.4.0/decls/stylelint.js#L71-L85
 * @see github.com/stylelint/stylelint/blob/8.4.0/decls/stylelint.js#L63-L69
 */
function isStylelintResponseSyntaxError(stylelintResponse) {

    return stylelintResponse.results.some(function (result) {
        return result.warnings.some(function (warning) {
            return (warning.rule === CSS_SYNTAX_ERROR);
        });
    });

}

function resolveGlobsToFilepaths(globs) {

    // github.com/isaacs/node-glob/tree/v7.1.2#usage
    const promises = globs.map((glob) => promisifiedNodeGlob(glob));

    return Promise.all(promises)
        .then(function (filepathsArrays) {                    // filepathsArrays is a 2D array
            const filepaths = [].concat(...filepathsArrays);  // Flatten it to a 1D array
            return filepaths;
        });

}

function runToolOnResolvedGlobs(globs, runToolOnSingleFilepath) {

    return resolveGlobsToFilepaths(globs)
        .then(function (filepaths) {
            const promises = filepaths.map((filepath) => runToolOnSingleFilepath(filepath));
            return Promise.all(promises);
        });

}


//----------------------------------------------------------------------//
//                             Tool runners                             //
//----------------------------------------------------------------------//

function runStylelintCheck(globs) {

    const options = {  // stylelint.io/user-guide/node-api/#options
        files:     globs,
        syntax:    STYLELINT_SYNTAX,
        formatter: STYLELINT_FORMATTER
    };

    return stylelint.lint(options)  // stylelint.io/user-guide/node-api/
        .then(function (response) {
            if (response.errored) {  // stylelint.io/user-guide/node-api/#errored
                console.log(chalk.red('Linting problems:'));
                console.log(response.output);  // stylelint.io/user-guide/node-api/#output
                return Promise.reject(new Error());
            } else {
                console.log(chalk.green('No linting problems!'));
                return Promise.resolve();
            }
        });

}

function runStylelintFixSingle(filepath) {

    let originalCode = null;

    // nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_readfile_path_options_callback
    return promisifiedReadFile(filepath, STYLELINT_INPUT_ENCODING)
        .then((thisOriginalCode) => { originalCode = thisOriginalCode; })
        .then(() => stylelint.lint({  // stylelint.io/user-guide/node-api/
            code:         originalCode,
            codeFilename: filepath,
            syntax:       STYLELINT_SYNTAX,
            formatter:    STYLELINT_FORMATTER,
            fix:          true
        }))
        .then(function (response) {
            if (isStylelintResponseSyntaxError(response)) {
                return Promise.reject(new Error(response.output));  // stylelint.io/user-guide/node-api/#output
            }
            // github.com/stylelint/stylelint/blob/8.4.0/lib/standalone.js#L161-L164
            // github.com/stylelint/stylelint/pull/2787#issuecomment-321279998
            const fixedCode     = response.output;
            const wereFixesMade = (fixedCode !== originalCode);
            // nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_writefile_file_data_options_callback
            return conditionalExecute(wereFixesMade, () => promisifiedWriteFile(filepath, fixedCode));
        });

}

function runStylelintFix(globs) {

    return runToolOnResolvedGlobs(globs, runStylelintFixSingle)
        .then(() => console.log(chalk.gray('Finished fixing SCSS')));

}

function runNodeSassSingle(inputFilepath) {

    const outputFilepath = inputFilepath.replace(
        COMPILER_FILENAME_EXT_INPUT,
        COMPILER_FILENAME_EXT_OUTPUT
    );

    const options = {  // github.com/sass/node-sass/tree/v4.7.2#options
        file: inputFilepath
    };

    // github.com/sass/node-sass/tree/v4.7.2#usage
    // github.com/sass/node-sass/tree/v4.7.2#result-object
    // nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_writefile_file_data_options_callback
    return promisifiedNodeSass(options)
        .then((result) => promisifiedWriteFile(outputFilepath, result.css));

}

function runNodeSass(globs) {

    return runToolOnResolvedGlobs(globs, runNodeSassSingle)
        .then(() => console.log(chalk.gray('Finished compiling SCSS to CSS')));

}


//----------------------------------------------------------------------//
//                            Exported class                            //
//----------------------------------------------------------------------//

class SCSSPipeline {

    constructor() {

        this.busyWatchFiles = {};

    }

    execute(globs, actions) {

        return          conditionalExecute(actions.fix,     () => runStylelintFix(globs))
            .then(() => conditionalExecute(actions.lint,    () => runStylelintCheck(globs)))
            .then(() => conditionalExecute(actions.compile, () => runNodeSass(globs)))
            .then(() => console.log(''))
            .catch(function (err) {
                console.log(err.message);
                return Promise.reject(new Error());
            });

    }

    watch(globs, actions) {

        console.log(chalk.blue([
            'Watching files:',
            ...globs.map((glob) => '    ' + glob),
            ''
        ].join('\n')));

        chokidar.watch(globs)  // github.com/paulmillr/chokidar/tree/2.0.0#api
            .on('change', (pathChangedFile) => {

                if (this.busyWatchFiles[pathChangedFile]) {
                    return;  // We're currently busy processing this file, so ignore this watch event.
                }

                this.busyWatchFiles[pathChangedFile] = true;

                // nodejs.org/dist/v8.9.0/docs/api/tty.html#tty_writestream_columns
                console.log('-'.repeat(process.stdout.columns));

                this.execute([pathChangedFile], actions)
                    .catch(() => null)  // Ignore any errors.
                    .then(() => { this.busyWatchFiles[pathChangedFile] = false; });

            })
            .on('error', (err) => console.log('ERROR:', err));

    }

}

module.exports = SCSSPipeline;  // nodejs.org/dist/v8.9.0/docs/api/modules.html#modules_module_exports
